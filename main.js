var ballRadius = 20;
var x = 480 / 2;
var y = 320 / 2 - 30;
var dx = -3;
var dy = 3;
var paddleHeight = 10;
var paddleWidth = 75;
var bounce = false;
var canvasHeight = 320;
var canvasWidth = 480;
var brickColumns = 5;
var brickRows = 3;
var brickWidth = 75;
var brickHeight = 20;
var brickPadding = 10;
var brickSpaceLeft = 30;
var brickSpaceTop = 30;
var brickNumber = brickColumns*brickRows;
var score = 0;
var lives = 3;

var bricks = [];
for (var i = 0; i < brickColumns; i++) {
    bricks[i] = [];
    for (var j = 0; j < brickRows; j++) {
        bricks[i][j] = { x: 0, y: 0, status: 1 };
    }
}
function setup() {
    createCanvas(canvasWidth, canvasHeight);
    frameRate(60);
}

function draw() {
    background(255);
    drawBall();
    drawPlate();
    drawBricks();
    brickCollision();
    drawScore();

}

function drawScore(){
    fill(120,102,243);
    textSize(20);
    text("SCORE: "+score,10, 20);
}

function brickCollision(){
    for (var c = 0; c < brickColumns; c++) {
        for (var r = 0; r < brickRows; r++) {
            var b = bricks[c][r];
            // calcul
            if (b.status == 1) {
                if (x > b.x && x < b.x + brickWidth && y > b.y && y < b.y + brickHeight+10) {
                    dy = -dy;
                    b.status = 0;
                    brickNumber--;
                    score++;
                }
            }
        }
        
    }
    if (brickNumber == 0) {
            alert('YOU WON');
        }
}

function drawBall() {
    fill(230);
    ellipse(x, y, ballRadius, ballRadius);

    if (x + dx > width - ballRadius || x + dx < ballRadius) {
        dx = -dx;
    }

    if (y + dy < ballRadius) {
        dy = -dy;
    }
    if (y + dy > height - ballRadius) {
        if (x > mouseX && x < mouseX + paddleWidth) {
            dy = -dy;
        } else {
               alert('GAME OVER, YOUR SCORE IS '+score); 
        }
    }

    x += dx;
    y += dy;
}

function drawBricks() {
    for (var c = 0; c < brickColumns; c++) {
        for (var r = 0; r < brickRows; r++) {
            if (bricks[c][r].status == 1) {
                var brickX = (c * (brickWidth + brickPadding)) + brickSpaceLeft;
                var brickY = (r * (brickHeight + brickPadding)) + brickSpaceTop;
                bricks[c][r].x = brickX;
                bricks[c][r].y = brickY;
            rect(brickX, brickY, brickWidth, brickHeight);
            }
        }
    }
}

function drawPlate() {
    fill(237, 34, 93);
    rect(mouseX, height - paddleHeight, paddleWidth, paddleHeight);
}